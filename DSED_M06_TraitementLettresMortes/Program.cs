﻿using RabbitMQ.Client;
using Newtonsoft.Json;
using DSED_M06_ComptesBancaires_FileMessages;

namespace DSED_M06_TraitementLettresMortes
{
    class Program
    {
        static void Main(string[] args)
        {
            Traitement traitement = new Traitement("localhost");
            traitement.TraiterMessage();
        }
    }
}