﻿using RabbitMQ.Client;
using Newtonsoft.Json;
using DSED_M06_ComptesBancaires_FileMessages;
using System.Text;
using RabbitMQ.Client.Events;
using System.IO;

namespace DSED_M06_TraitementLettresMortes
{
    public class Traitement
    {
        private string? Path = Directory.GetParent(Environment.CurrentDirectory)?.Parent?.Parent?.Parent?.GetDirectories("TransactionsEnErreurs")?.Select(directory => directory)?
                                                                                                         .SingleOrDefault()?.FullName;
        private ManualResetEvent m_waitHandle;
        private ConnectionFactory m_connectionFactory;
        public Traitement(string p_host)
        {
            if (p_host == null || p_host.Length < 1)
            {
                throw new ArgumentNullException(nameof(p_host));
            }

            this.m_waitHandle = new ManualResetEvent(false);
            this.m_connectionFactory = new ConnectionFactory() { HostName = p_host };
        }
        public void TraiterMessage()
        {
            using (IConnection connexion = this.m_connectionFactory.CreateConnection())
            {
                using (IModel channel = connexion.CreateModel())
                {
                    channel.QueueDeclare(queue: "m06-comptes-lettres-mortes", durable: false, exclusive: false,
                    autoDelete: false, arguments: null
                    );

                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Out.WriteLine("=== File de message: m06-comptes-lettres-mortes ===");
                    Console.Out.WriteLine();

                    EventingBasicConsumer consommateur = new EventingBasicConsumer(channel);
                    consommateur.Received += (model, ea) =>
                    {
                        byte[] donnees = ea.Body.ToArray();
                        string enveloppeLettreMorteJSON = Encoding.UTF8.GetString(donnees);

                        EnveloppeLettreMorte? enveloppeLettreMorte = JsonConvert.DeserializeObject<EnveloppeLettreMorte>(enveloppeLettreMorteJSON);

                        if (enveloppeLettreMorte != null)
                        {
                            Infos infos = new Infos();
                            infos.Id = enveloppeLettreMorte.Id;
                            infos.Date = enveloppeLettreMorte.Date;
                            infos.Description = enveloppeLettreMorte.Description.Split("; ", StringSplitOptions.None);

                            string infosJSON = JsonConvert.SerializeObject(infos);
                            string nomFichier = enveloppeLettreMorte.Date.ToString().Replace(":", "-") + "-" + enveloppeLettreMorte.Id.ToString();

                            using (StreamWriter file = new(this.Path + "\\" + nomFichier + ".json", append: true))
                            {
                                file.WriteAsync(infosJSON);
                            }

                            using (BinaryWriter file = new BinaryWriter(File.OpenWrite(this.Path + "\\" + nomFichier + ".bin")))
                            {
                                file.Write(Encoding.ASCII.GetBytes(donnees.ToString()));
                            }

                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.Out.WriteLine("{0}\tLettre morte traitee, ID: {1}", DateTime.Now.ToShortTimeString(), enveloppeLettreMorte.Id.ToString());
                        }

                        Console.Error.WriteLine("{0}\tErreur traitement lettre morte!", DateTime.Now.ToShortTimeString());

                        channel.BasicAck(ea.DeliveryTag, false);
                    };
                    channel.BasicConsume(queue: "m06-comptes-lettres-mortes",
                    autoAck: false,
                    consumer: consommateur
                    );
                    this.m_waitHandle.WaitOne();
                }
            }
        }
    }
}
