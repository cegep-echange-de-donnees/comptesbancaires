﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSED_M06_TraitementLettresMortes
{
    public class Infos
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string[] Description { get; set; }
        public Infos()
        {
            ;
        }
    }
}
