DROP TABLE IF EXISTS Compte;
CREATE TABLE Compte(
	compteId UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID()
	,type VARCHAR(20) NOT NULL
);

DROP TABLE IF EXISTS [Transaction];
CREATE TABLE [Transaction](
	transactionId UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID()
	,numeroCompte UNIQUEIDENTIFIER REFERENCES Compte (CompteId) NOT NULL
	,type VARCHAR(10) NOT NULL
	,date DATE NOT NULL DEFAULT GETDATE()
	,montant DECIMAL(7,2) NOT NULL
	,CHECK (montant >= 0.00)
);

SELECT * FROM Compte;
SELECT * FROM [Transaction];

INSERT INTO Compte(type) VALUES('courant');