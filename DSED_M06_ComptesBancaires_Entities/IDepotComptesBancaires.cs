﻿namespace DSED_M06_ComptesBancaires_Entities
{
    public interface IDepotComptesBancaires
    {
        public IEnumerable<Compte> ObtenirListeComptesBancaires();
        public Compte RechercherCompteBancaire(Guid p_numeroCompte);
        public void CreerCompteBancaire(Compte p_compte);
        public void ModifierCompteBancaire(Compte p_compte);
        public IEnumerable<Transaction> ObtenirListeTransactions();
        public Transaction RechercherTransaction(Guid p_id);
        public void CreerTransaction(Transaction p_transaction);
    }
}
