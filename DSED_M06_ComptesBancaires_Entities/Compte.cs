﻿namespace DSED_M06_ComptesBancaires_Entities
{
    public class Compte
    {
        public Guid Numero { get; private set; }
        public string Type { get; private set; }
        public List<Transaction> Transactions { get; set; }
        public Compte(string p_typeCompte)
        {
            if (p_typeCompte == null)
            {
                throw new ArgumentNullException(nameof(p_typeCompte));
            }
            if (p_typeCompte != "courant")
            {
                throw new InvalidDataException(nameof(p_typeCompte));
            }

            this.Type = p_typeCompte;
            this.Transactions = new List<Transaction>();
        }
        public Compte(Guid p_numeroCompte, string p_typeCompte) : this(p_typeCompte)
        {
            if (p_numeroCompte == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(p_numeroCompte));
            }

            this.Numero = p_numeroCompte;
        }
        public override bool Equals(object? obj)
        {
            if (obj is not Compte)
            {
                return false;
            }

            Compte? compte = obj as Compte;

            return this.Numero == compte?.Numero;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(this.Numero.GetHashCode(), this.Type.GetHashCode());
        }
        public override string ToString()
        {
            string compte = $"Numéro de compte: {this.Numero}\r\nType de compte: {this.Type}\r\n"
                            + $"{(this.Transactions.Count() > 0 ? "Transactions: " : "Aucune transaction")} ";

            if (this.Transactions.Count > 0)
            {
                foreach (Transaction transaction in this.Transactions)
                {
                    compte = compte + Environment.NewLine + transaction.ToString();
                }
            }

            return compte;
        }
    }
}
