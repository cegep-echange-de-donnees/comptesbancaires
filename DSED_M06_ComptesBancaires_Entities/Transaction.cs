﻿namespace DSED_M06_ComptesBancaires_Entities
{
    public class Transaction
    {
        public Guid Id { get; private set; }
        public Guid NumeroCompte { get; private set; }
        public string Type { get; private set; }
        public DateTime? Date { get; private set; }
        public decimal Montant { get; private set; }
        public Transaction(Guid p_numeroCompte, string p_type, DateTime? p_date, decimal p_montant)
        {
            if (p_numeroCompte == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(p_numeroCompte));
            }
            if (p_type == null || p_type.Length < 1)
            {
                throw new ArgumentNullException(nameof(p_type));
            }
            if (p_type != "credit" && p_type != "debit")
            {
                throw new InvalidDataException(nameof(p_type));
            }
            if (p_date != null && p_date > DateTime.Now)
            {
                throw new ArgumentOutOfRangeException(nameof(p_date));
            }
            if (Montant < 0.00m)
            {
                throw new ArgumentOutOfRangeException(nameof(p_montant));
            }

            this.NumeroCompte = p_numeroCompte;
            this.Type = p_type;

            if (p_date == null)
            {
                this.Date = DateTime.Now;
            }
            else
            {
                this.Date = p_date.Value;
            }

            this.Montant = p_montant;
        }
        public Transaction(Guid p_id, Guid p_numeroCompte, string p_type, DateTime? p_date, decimal p_montant) : this(p_numeroCompte, p_type, p_date, p_montant)
        {
            if (p_id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(p_id));
            }

            this.Id = p_id;
        }
        public override bool Equals(object? obj)
        {
            if (obj is not Transaction)
            {
                return false;
            }

            Transaction? transaction = obj as Transaction;

            return this.Id == transaction?.Id;
        }
        public override int GetHashCode()
        {
            return HashCode.Combine(this.Id.GetHashCode(), this.NumeroCompte, this.Type.GetHashCode(), this.Date.GetHashCode());
        }
        public override string ToString()
        {
            return $"Numéro de transaction: {this.Id}\r\n Numéro de Compte: {this.NumeroCompte}\r\n Type de transaction: {this.Type}\r\n"
                   + $"Date de la transaction: {this.Date}\r\n + Montant: {this.Montant}";
        }
    }
}
