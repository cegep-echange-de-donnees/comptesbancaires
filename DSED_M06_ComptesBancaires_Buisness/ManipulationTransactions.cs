﻿using DSED_M06_ComptesBancaires_Entities;
using DSED_M06_ComptesBancaires_DTO;
using DSED_M06_ComptesBancaires_FileMessages;

namespace DSED_M06_ComptesBancaires_Buisness
{
    public class ManipulationTransactions
    {
        private IDepotComptesBancaires m_depotComptesBancaires;
        private FileMessagesProducteur m_messagesProducteur;
        public ManipulationTransactions(IDepotComptesBancaires p_depotComptesBancaires, FileMessagesProducteur p_fileMessagesProducteur)
        {
            if (p_depotComptesBancaires == null)
            {
                throw new ArgumentNullException(nameof(p_depotComptesBancaires));
            }
            if (p_fileMessagesProducteur == null)
            {
                throw new ArgumentNullException(nameof(p_fileMessagesProducteur));
            }

            this.m_messagesProducteur = p_fileMessagesProducteur;
            this.m_depotComptesBancaires = p_depotComptesBancaires;
        }
        public void CreerTransaction(DSED_M06_ComptesBancaires_Entities.Transaction p_transaction)
        {
            if (p_transaction == null)
            {
                throw new ArgumentNullException("Transaction est nulle");
            }
            if (p_transaction.NumeroCompte == Guid.Empty)
            {
                throw new ArgumentNullException("Numero de la transaction est nul");
            }
            if (p_transaction.Type == null || p_transaction.Type.Length < 1)
            {
                throw new ArgumentNullException("Type de la transaction est nulle");
            }
            if (p_transaction.Type != "credit" && p_transaction.Type != "debit")
            {
                throw new InvalidDataException("Type de transaction inconnu");
            }
            if (p_transaction.Date != DateTime.MinValue && p_transaction.Date > DateTime.Now)
            {
                throw new ArgumentOutOfRangeException("Date de la transaction invalide");
            }
            if (p_transaction.Montant < 0.00m)
            {
                throw new ArgumentOutOfRangeException("Montant de la transaction invalide");
            }

            DSED_M06_ComptesBancaires_Entities.Compte compteBancaire = this.m_depotComptesBancaires.RechercherCompteBancaire(p_transaction.NumeroCompte);

            if(compteBancaire == null)
            {
                throw new ArgumentNullException("Compte bancaire inconnu");
            }

            Guid transactionId = Guid.NewGuid();
            bool dateAChanger = false;
            DateTime date = new DateTime();

            if (p_transaction.Date == null || p_transaction.Date == DateTime.MinValue)
            {
                date = DateTime.Now;
                dateAChanger = true;
            }

            DSED_M06_ComptesBancaires_Entities.Transaction transaction = new DSED_M06_ComptesBancaires_Entities.Transaction(
                                                                        transactionId, p_transaction.NumeroCompte, p_transaction.Type
                                                                        ,dateAChanger ? date : p_transaction.Date
                                                                        ,p_transaction.Montant);

            this.m_depotComptesBancaires.CreerTransaction(transaction);
        }
        public IEnumerable<DSED_M06_ComptesBancaires_Entities.Transaction> ObtenirListeTransactions()
        {
            return this.m_depotComptesBancaires.ObtenirListeTransactions();
        }
        public DSED_M06_ComptesBancaires_Entities.Transaction RechercherTransaction(Guid p_id)
        {
            if (p_id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(p_id));
            }

            return this.m_depotComptesBancaires.RechercherTransaction(p_id);
        }
        public DSED_M06_ComptesBancaires_Entities.Compte RechercherCompteBancaire(Guid p_numeroCompte)
        {
            if (p_numeroCompte == Guid.Empty)
            {
                throw new ArgumentNullException("Numero de compte bancaire nul");
            }

            return this.m_depotComptesBancaires.RechercherCompteBancaire(p_numeroCompte);
        }
        public void EnvoyerMessageTransaction(DSED_M06_ComptesBancaires_Entities.Transaction p_transaction, string p_string)
        {
            if (p_transaction == null)
            {
                throw new ArgumentNullException(nameof(p_transaction));
            }
            if (p_transaction.NumeroCompte == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(p_transaction.NumeroCompte));
            }
            if (p_transaction.Type == null || p_transaction.Type.Length < 1)
            {
                throw new ArgumentNullException(nameof(p_transaction.Type));
            }
            if (p_transaction.Type != "credit" && p_transaction.Type != "debit")
            {
                throw new InvalidDataException(nameof(p_transaction.Type));
            }
            if (p_transaction.Date != DateTime.MinValue && p_transaction.Date > DateTime.Now)
            {
                throw new ArgumentOutOfRangeException(nameof(p_transaction.Date));
            }
            if (p_transaction.Montant < 0.00m)
            {
                throw new ArgumentOutOfRangeException(nameof(p_transaction.Montant));
            }
            if (p_string == null || p_string.Length < 1)
            {
                throw new ArgumentNullException(nameof(p_string));
            }
            if (p_string != "POST")
            {
                throw new InvalidDataException(nameof(p_string));
            }

            DSED_M06_ComptesBancaires_DTO.Transaction transaction = new DSED_M06_ComptesBancaires_DTO.Transaction();
            transaction.NumeroCompte = new Guid(p_transaction.NumeroCompte.ToString());
            transaction.Type = p_transaction.Type;
            transaction.Date = p_transaction.Date;
            transaction.Montant = p_transaction.Montant;
            this.m_messagesProducteur.EnvoyerMessageTransaction(transaction, p_string);
        }
    }
}
