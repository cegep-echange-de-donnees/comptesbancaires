﻿using DSED_M06_ComptesBancaires_Entities;
using DSED_M06_ComptesBancaires_DTO;
using DSED_M06_ComptesBancaires_FileMessages;

namespace DSED_M06_ComptesBancaires_Buisness
{
    public class ManipulationComptes
    {
        private IDepotComptesBancaires m_depotComptesBancaires;
        private FileMessagesProducteur m_messagesProducteur;
        public ManipulationComptes(IDepotComptesBancaires p_depotComptesBancaires, FileMessagesProducteur p_fileMessagesProducteur)
        {
            if(p_depotComptesBancaires == null)
            {
                throw new ArgumentNullException(nameof(p_depotComptesBancaires));
            }
            if (p_fileMessagesProducteur == null)
            {
                throw new ArgumentNullException(nameof(p_fileMessagesProducteur));
            }

            this.m_messagesProducteur = p_fileMessagesProducteur;
            this.m_depotComptesBancaires = p_depotComptesBancaires;
        }
        public void CreerCompteBancaire(DSED_M06_ComptesBancaires_Entities.Compte p_compte)
        {
            if (p_compte == null)
            {
                throw new ArgumentNullException("Compte bancaire est nul");
            }
            if (p_compte.Type == null)
            {
                throw new ArgumentNullException("Type de compte est nul");
            }
            if (p_compte.Type != "courant")
            {
                throw new InvalidDataException("Type de compte inconnu");
            }

            Guid numeroCompte = Guid.NewGuid();
            DSED_M06_ComptesBancaires_Entities.Compte compte = new DSED_M06_ComptesBancaires_Entities.Compte(numeroCompte, p_compte.Type);

            this.m_depotComptesBancaires.CreerCompteBancaire(compte);
        }

        public void ModifierCompteBancaire(DSED_M06_ComptesBancaires_Entities.Compte p_compte)
        {
            if (p_compte == null)
            {
                throw new ArgumentNullException("Compte bancaire est nul");
            }
            if (p_compte.Type == null)
            {
                throw new ArgumentNullException("Type de compte est nul");
            }
            if (p_compte.Type != "courant")
            {
                throw new InvalidDataException("Type de compte inconnu");
            }
            if(p_compte.Numero == Guid.Empty)
            {
                throw new ArgumentNullException("Le numero de compte est invalide.");
            }

            DSED_M06_ComptesBancaires_Entities.Compte compteEntite = this.RechercherCompteBancaire(p_compte.Numero);

            if (compteEntite == null)
            {
                throw new ArgumentNullException("Compte bancaire non trouve");
            }

            this.m_depotComptesBancaires.ModifierCompteBancaire(p_compte);
        }

        public IEnumerable<DSED_M06_ComptesBancaires_Entities.Compte> ObtenirListeComptesBancaires()
        {
            return this.m_depotComptesBancaires.ObtenirListeComptesBancaires();
        }

        public DSED_M06_ComptesBancaires_Entities.Compte RechercherCompteBancaire(Guid p_numeroCompte)
        {
            if (p_numeroCompte == Guid.Empty)
            {
                throw new ArgumentNullException("Numero de compte bancaire nul");
            }

            return this.m_depotComptesBancaires.RechercherCompteBancaire(p_numeroCompte);
        }
        public void EnvoyerMessageCompte(DSED_M06_ComptesBancaires_Entities.Compte p_compte, string p_string)
        {
            if (p_compte == null)
            {
                throw new ArgumentNullException(nameof(p_compte));
            }
            if (p_compte.Type == null)
            {
                throw new ArgumentNullException(nameof(p_compte.Type));
            }
            if (p_compte.Type != "courant")
            {
                throw new InvalidDataException(nameof(p_compte.Type));
            }
            if(p_string == null || p_string.Length < 1)
            {
                throw new ArgumentNullException(nameof(p_string));
            }
            if(p_string != "POST" && p_string != "PUT")
            {
                throw new InvalidDataException(nameof(p_string));
            }

            if(p_string == "PUT")
            {
                DSED_M06_ComptesBancaires_Entities.Compte compteExiste = this.RechercherCompteBancaire(p_compte.Numero);

                if(compteExiste == null)
                {
                    throw new ArgumentNullException(nameof(p_compte.Numero));
                }
            }

            DSED_M06_ComptesBancaires_DTO.Compte compteDTO = new DSED_M06_ComptesBancaires_DTO.Compte();

            if(p_string == "PUT")
            {
                compteDTO.Numero = new Guid(p_compte.Numero.ToString());
            }

            compteDTO.Type = p_compte.Type;
            this.m_messagesProducteur.EnvoyerMessageCompte(compteDTO, p_string);
        }
    }
}
