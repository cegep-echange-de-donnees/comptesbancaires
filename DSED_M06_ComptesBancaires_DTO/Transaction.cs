﻿namespace DSED_M06_ComptesBancaires_DTO
{
    public class Transaction
    {
        public Guid Id { get; set; }
        public Guid NumeroCompte { get; set; }
        public string Type { get; set; }
        public DateTime? Date { get; set; }
        public decimal Montant { get; set; }
        public Transaction()
        {
            ;
        }
    }
}
