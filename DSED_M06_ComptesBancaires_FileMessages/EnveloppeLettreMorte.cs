﻿using DSED_M06_ComptesBancaires_FileMessages;
using DSED_M06_ComptesBancaires_FileMessages.Messages;

namespace DSED_M06_ComptesBancaires_FileMessages
{
    public class EnveloppeLettreMorte
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public byte[] Donnees { get; set; }
        public EnveloppeLettreMorte()
        {
            ;
        }
    }
}
