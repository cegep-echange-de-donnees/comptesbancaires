﻿using RabbitMQ.Client;
using Newtonsoft.Json;
using System.Text;
using DSED_M06_ComptesBancaires_DTO;
using DSED_M06_ComptesBancaires_FileMessages.Messages;

namespace DSED_M06_ComptesBancaires_FileMessages
{
    public class FileMessagesProducteur
    {
        private ConnectionFactory m_factory;
        public FileMessagesProducteur(string p_hostname)
        {
            if (p_hostname == null || p_hostname.Length < 1)
            {
                throw new ArgumentNullException(p_hostname);
            }

            m_factory = new ConnectionFactory() { HostName = p_hostname };
        }
        #region Enveloppe m06-comptes
        private void CreerEnveloppe(IMessage p_message, string p_action, Enveloppe p_enveloppe)
        {
            p_enveloppe.Action = p_action;
            p_enveloppe.ActionId = Guid.NewGuid();

            if(p_enveloppe.TypeMessage == TypeMessage.COMPTE)
            {
                p_enveloppe.MessageCompte = p_message as MessageCompte;
            }
            else if(p_enveloppe.TypeMessage == TypeMessage.TRANSACTION)
            {
                p_enveloppe.MessageTransaction = p_message as MessageTransaction;
            }
        }
        #endregion
        #region Message compte
        private MessageCompte CreerMessageCompte(Compte p_compteBancaire)
        {
            MessageCompte messageCompte = new MessageCompte();
            messageCompte.Type = p_compteBancaire.Type;

            return messageCompte;
        }
        public void EnvoyerMessageCompte(Compte p_compteBancaire, string p_action)
        {
            if (p_compteBancaire == null)
            {
                throw new ArgumentNullException(nameof(p_compteBancaire));
            }
            if (p_compteBancaire.Type == null)
            {
                throw new ArgumentNullException(nameof(p_compteBancaire.Type));
            }
            if (p_compteBancaire.Type != "courant")
            {
                throw new InvalidDataException(nameof(p_compteBancaire.Type));
            }
            if (p_action == null || (p_action != "POST" && p_action != "PUT"))
            {
                throw new InvalidOperationException(nameof(p_action));
            }

            MessageCompte messageCompte = this.CreerMessageCompte(p_compteBancaire);

            if(p_action == "PUT")
            {
                messageCompte.Numero = new Guid(p_compteBancaire.Numero.ToString());
            }

            Enveloppe enveloppe = new Enveloppe();
            enveloppe.TypeMessage = TypeMessage.COMPTE;
            this.CreerEnveloppe(messageCompte, p_action, enveloppe);

            string enveloppeJSON = JsonConvert.SerializeObject(enveloppe);
            this.Executer(enveloppeJSON, "m06-comptes");
        }
        #endregion
        #region Message transaction
        private MessageTransaction CreerMessageTransaction(Transaction p_transaction)
        {
            MessageTransaction messageTransaction = new MessageTransaction();
            messageTransaction.NumeroCompte = new Guid(p_transaction.NumeroCompte.ToString());
            messageTransaction.Type = p_transaction.Type;
            messageTransaction.Date = p_transaction.Date;
            messageTransaction.Montant = p_transaction.Montant;

            return messageTransaction;
        }
        public void EnvoyerMessageTransaction(Transaction p_transaction, string p_action)
        {
            if (p_transaction == null)
            {
                throw new ArgumentNullException(nameof(p_transaction));
            }
            if (p_transaction.NumeroCompte == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(p_transaction.NumeroCompte));
            }
            if (p_transaction.Type == null || p_transaction.Type.Length < 1)
            {
                throw new ArgumentNullException(nameof(p_transaction.Type));
            }
            if (p_transaction.Type != "credit" && p_transaction.Type != "debit")
            {
                throw new InvalidDataException(nameof(p_transaction.Type));
            }
            if (p_transaction.Date != DateTime.MinValue && p_transaction.Date > DateTime.Now)
            {
                throw new ArgumentOutOfRangeException(nameof(p_transaction.Date));
            }
            if (p_transaction.Montant < 0.00m)
            {
                throw new ArgumentOutOfRangeException(nameof(p_transaction.Montant));
            }
            if (p_action == null || (p_action != "POST" && p_action != "PUT"))
            {
                throw new InvalidOperationException(nameof(p_action));
            }

            MessageTransaction messageTransaction = this.CreerMessageTransaction(p_transaction);
            Enveloppe enveloppe = new Enveloppe();
            enveloppe.TypeMessage = TypeMessage.TRANSACTION;
            this.CreerEnveloppe(messageTransaction, p_action, enveloppe);

            string enveloppeJSON = JsonConvert.SerializeObject(enveloppe);
            this.Executer(enveloppeJSON, "m06-comptes");
        }
        #endregion
        #region Lettre morte
        public void EnvoyerLettreMorte(EnveloppeLettreMorte p_enveloppeLettreMorte)
        {
            if(p_enveloppeLettreMorte == null)
            {
                throw new ArgumentNullException(nameof(p_enveloppeLettreMorte));
            }
            if(p_enveloppeLettreMorte.Id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(p_enveloppeLettreMorte.Id));
            }
            if(p_enveloppeLettreMorte.Date == DateTime.MinValue)
            {
                throw new ArgumentOutOfRangeException(nameof(p_enveloppeLettreMorte.Date));
            }

            string enveloppeLettreMorteJSON = JsonConvert.SerializeObject(p_enveloppeLettreMorte);
            this.Executer(enveloppeLettreMorteJSON, "m06-comptes-lettres-mortes");
        }
        public void CompleterLettreMorteBase(EnveloppeLettreMorte p_enveloppeLettreMorte)
        {
            if(p_enveloppeLettreMorte == null)
            {
                throw new ArgumentNullException(nameof(p_enveloppeLettreMorte));
            }

            p_enveloppeLettreMorte.Id = Guid.NewGuid();
            p_enveloppeLettreMorte.Date = DateTime.Now;
        }
        #endregion
        private void Executer(string p_enveloppeJSON, string p_file)
        {
            using (IConnection connexion = m_factory.CreateConnection())
            {
                using (IModel channel = connexion.CreateModel())
                {
                    channel.QueueDeclare(queue: p_file,
                    durable: false,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null
                    );

                    byte[] body = Encoding.UTF8.GetBytes(p_enveloppeJSON);
                    channel.BasicPublish(exchange: "", routingKey: p_file, body: body);
                }
            }
        }
    }
}
