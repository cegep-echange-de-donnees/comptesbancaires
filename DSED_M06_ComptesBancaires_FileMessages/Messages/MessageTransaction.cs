﻿namespace DSED_M06_ComptesBancaires_FileMessages.Messages
{
    public class MessageTransaction : IMessage
    {
        public Guid NumeroCompte { get; set; }
        public string Type { get; set; }
        public DateTime? Date { get; set; }
        public decimal Montant { get; set; }
        public MessageTransaction()
        {
            ;
        }
    }
}
