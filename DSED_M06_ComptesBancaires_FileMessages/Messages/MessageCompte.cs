﻿namespace DSED_M06_ComptesBancaires_FileMessages.Messages
{
    public class MessageCompte : IMessage
    {
        public Guid Numero { get; set; }
        public string Type { get; set; }
        public MessageCompte()
        {
            ;
        }
    }
}