﻿using DSED_M06_ComptesBancaires_FileMessages.Messages;

namespace DSED_M06_ComptesBancaires_FileMessages
{
    public class Enveloppe
    {
        public string Action { get; set; }
        public Guid ActionId { get; set; }
        public TypeMessage TypeMessage { get; set; }
        public MessageCompte? MessageCompte { get; set; }
        public MessageTransaction? MessageTransaction { get; set; }
        public Enveloppe()
        {
            ;
        }
    }
}
