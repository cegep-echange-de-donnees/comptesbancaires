﻿using RabbitMQ.Client;
using Newtonsoft.Json;
using DSED_M06_ComptesBancaires_FileMessages;
using DSED_M06_ComptesBancaires_API.Data;
using RabbitMQ.Client.Events;
using System.Text;
using DSED_M06_TraitementCreationsModifications.Traitements;

namespace DSED_M06_TraitementCreationsModifications
{
    public class Traitement
    {
        private ManualResetEvent m_waitHandle;
        private ConnectionFactory m_connectionFactory;
        private ApplicationDbContext m_context;
        private TraitementComptes m_traitementComptes;
        private TraitementTransactions m_traitementTransactions;
        private FileMessagesProducteur m_messagesProducteur;
        public Traitement(ApplicationDbContext p_applicationDbContext, TraitementComptes p_traitementComptes, TraitementTransactions p_traitementTransactions, FileMessagesProducteur p_messagesProducteur, string p_host)
        {
            if(p_applicationDbContext == null)
            {
                throw new ArgumentNullException(nameof(p_applicationDbContext));
            }
            if(p_traitementComptes == null)
            {
                throw new ArgumentNullException(nameof(p_traitementComptes));
            }
            if(p_traitementTransactions == null)
            {
                throw new ArgumentNullException(nameof(p_traitementTransactions));
            }
            if(p_messagesProducteur == null)
            {
                throw new ArgumentNullException(nameof(p_messagesProducteur));
            }
            if(p_host == null || p_host.Length < 1)
            {
                throw new ArgumentNullException(nameof(p_host));
            }

            this.m_waitHandle = new ManualResetEvent(false);
            this.m_connectionFactory = new ConnectionFactory() { HostName = p_host };
            this.m_context = p_applicationDbContext;
            this.m_traitementComptes = p_traitementComptes;
            this.m_traitementTransactions = p_traitementTransactions;
            this.m_messagesProducteur = p_messagesProducteur;
        }
        public void TraiterMessages()
        {
            using (this.m_context)
            {
                using (IConnection connexion = this.m_connectionFactory.CreateConnection())
                {
                    using (IModel channel = connexion.CreateModel())
                    {
                        channel.QueueDeclare(queue: "m06-comptes", durable: false, exclusive: false,
                        autoDelete: false, arguments: null
                        );

                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Out.WriteLine("=== File de message: m06-comptes ===");
                        Console.Out.WriteLine();

                        EventingBasicConsumer consommateur = new EventingBasicConsumer(channel);
                        consommateur.Received += (model, ea) =>
                        {
                            byte[] donnees = ea.Body.ToArray();
                            string enveloppeJSON = Encoding.UTF8.GetString(donnees);

                            Enveloppe? enveloppe = JsonConvert.DeserializeObject<Enveloppe>(enveloppeJSON);

                            EnveloppeLettreMorte enveloppeLettreMorte = new EnveloppeLettreMorte();
                            this.m_messagesProducteur.CompleterLettreMorteBase(enveloppeLettreMorte);

                            if (enveloppe != null)
                            {
                                if (enveloppe?.ActionId != Guid.Empty)
                                {
                                    if (enveloppe?.TypeMessage == TypeMessage.COMPTE)
                                    {
                                        this.m_traitementComptes.TraiterMessage(enveloppe, enveloppeLettreMorte);
                                    }
                                    else if (enveloppe?.TypeMessage == TypeMessage.TRANSACTION)
                                    {
                                        this.m_traitementTransactions.TraiterMessage(enveloppe, enveloppeLettreMorte);
                                    }
                                }
                                else
                                {
                                    enveloppeLettreMorte.Description = "ActionId non valide; ";
                                }
                            }
                            else
                            {
                                enveloppeLettreMorte.Description = "Enveloppe est nulle; ";
                            }

                            if (enveloppeLettreMorte.Description != null && enveloppeLettreMorte.Description.Length > 0)
                            {
                                enveloppeLettreMorte.Donnees = donnees;
                                this.m_messagesProducteur.EnvoyerLettreMorte(enveloppeLettreMorte);
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.Out.WriteLine("{0}\tErreur: Lettre morte envoyee, ID: {1}", DateTime.Now.ToShortTimeString(), enveloppeLettreMorte.Id.ToString());
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.Out.WriteLine("{0}\tMessage traité avec succes", DateTime.Now.ToShortTimeString());
                            }

                            channel.BasicAck(ea.DeliveryTag, false);
                        };
                        channel.BasicConsume(queue: "m06-comptes",
                        autoAck: false,
                        consumer: consommateur
                        );
                        this.m_waitHandle.WaitOne();
                    }
                }
            }
        }
    }
}
