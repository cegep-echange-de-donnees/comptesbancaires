﻿using DSED_M06_ComptesBancaires_Buisness;
using DSED_M06_ComptesBancaires_DTO;
using DSED_M06_ComptesBancaires_Entities;
using DSED_M06_ComptesBancaires_FileMessages;
using DSED_M06_ComptesBancaires_API.Data;
using DSED_M06_TraitementCreationsModifications.Traitements;

namespace DSED_M06_TraitementCreationsModifications
{
    class Program
    {
        static void Main(string[] args)
        {
            ApplicationDbContext context = DALDbContextGeneration.ObtenirApplicationDBContext();
            IDepotComptesBancaires depotComptesBancaires = new DepotComptesBancaires(context);
            FileMessagesProducteur fileMessagesProducteur = new FileMessagesProducteur("localhost");

            ManipulationComptes manipulationComptes = new ManipulationComptes(depotComptesBancaires, fileMessagesProducteur);
            ManipulationTransactions manipulationTransactions = new ManipulationTransactions(depotComptesBancaires, fileMessagesProducteur);

            TraitementComptes traitementComptes = new TraitementComptes(manipulationComptes);
            TraitementTransactions traitementTransactions = new TraitementTransactions(manipulationTransactions);

            Traitement traitement = new Traitement(context, traitementComptes, traitementTransactions, fileMessagesProducteur, "localhost");

            traitement.TraiterMessages();
        }
    }
}