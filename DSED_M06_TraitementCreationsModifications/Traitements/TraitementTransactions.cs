﻿using DSED_M06_ComptesBancaires_Buisness;
using DSED_M06_ComptesBancaires_FileMessages;
using DSED_M06_ComptesBancaires_FileMessages.Messages;

namespace DSED_M06_TraitementCreationsModifications.Traitements
{
    public class TraitementTransactions
    {
        private ManipulationTransactions m_manipulationTransactions;
        public TraitementTransactions(ManipulationTransactions p_manipulationTransactions)
        {
            if(p_manipulationTransactions == null)
            {
                throw new ArgumentNullException(nameof(p_manipulationTransactions));
            }

            this.m_manipulationTransactions = p_manipulationTransactions;
        }
        public void TraiterMessage(Enveloppe p_enveloppe, EnveloppeLettreMorte p_enveloppeLettreMorte)
        {
            if (p_enveloppe == null)
            {
                throw new ArgumentNullException(nameof(p_enveloppe));
            }
            if (p_enveloppeLettreMorte == null)
            {
                throw new ArgumentNullException(nameof(p_enveloppeLettreMorte));
            }

            DSED_M06_ComptesBancaires_DTO.Transaction transactionDTO = new DSED_M06_ComptesBancaires_DTO.Transaction();
            MessageTransaction? messageTransaction = p_enveloppe.MessageTransaction;

            if (messageTransaction != null)
            {
                transactionDTO.NumeroCompte = new Guid(messageTransaction.NumeroCompte.ToString());
                transactionDTO.Type = messageTransaction.Type;
                transactionDTO.Date = messageTransaction.Date;
                transactionDTO.Montant = messageTransaction.Montant;
                DSED_M06_ComptesBancaires_Entities.Transaction transaction = new DSED_M06_ComptesBancaires_Entities.Transaction(transactionDTO.NumeroCompte
                                                                                                                                , transactionDTO.Type
                                                                                                                                , transactionDTO.Date
                                                                                                                                , transactionDTO.Montant);

                if (p_enveloppe.Action == "POST")
                {
                    try
                    {
                        this.m_manipulationTransactions.CreerTransaction(transaction);
                    }
                    catch(ArgumentNullException exception)
                    {
                        p_enveloppeLettreMorte.Description = p_enveloppeLettreMorte.Description + exception.Message + "; ";
                    }
                    catch(InvalidDataException exception)
                    {
                        p_enveloppeLettreMorte.Description = p_enveloppeLettreMorte.Description + exception.Message + "; ";
                    }
                    catch(ArgumentOutOfRangeException exception)
                    {
                        p_enveloppeLettreMorte.Description = p_enveloppeLettreMorte.Description + exception.Message + "; ";
                    }
                }
                else
                {
                    p_enveloppeLettreMorte.Description = p_enveloppeLettreMorte.Description + "Action non valide; ";
                }
            }
            else if (p_enveloppe.MessageCompte != null)
            {
                p_enveloppeLettreMorte.Description = p_enveloppeLettreMorte.Description + "TypeMessage non coherent; ";
            }
            else
            {
                p_enveloppeLettreMorte.Description = p_enveloppeLettreMorte.Description + "Message est null; ";
            }
        }
    }
}
