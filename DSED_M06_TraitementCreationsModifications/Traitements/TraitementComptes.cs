﻿using DSED_M06_ComptesBancaires_Buisness;
using DSED_M06_ComptesBancaires_FileMessages;
using DSED_M06_ComptesBancaires_FileMessages.Messages;

namespace DSED_M06_TraitementCreationsModifications.Traitements
{
    public class TraitementComptes
    {
        private ManipulationComptes m_manipulationComptes;
        public TraitementComptes(ManipulationComptes p_manipulationComptes)
        {
            if(p_manipulationComptes == null)
            {
                throw new ArgumentNullException(nameof(p_manipulationComptes));
            }

            this.m_manipulationComptes = p_manipulationComptes;
        }
        public void TraiterMessage(Enveloppe p_enveloppe, EnveloppeLettreMorte p_enveloppeLettreMorte)
        {
            if(p_enveloppe == null)
            {
                throw new ArgumentNullException(nameof(p_enveloppe));
            }
            if(p_enveloppeLettreMorte == null)
            {
                throw new ArgumentNullException(nameof(p_enveloppeLettreMorte));
            }

            DSED_M06_ComptesBancaires_DTO.Compte compteDTO = new DSED_M06_ComptesBancaires_DTO.Compte();
            MessageCompte? messageCompte = p_enveloppe.MessageCompte;

            if (messageCompte != null)
            {
                compteDTO.Type = messageCompte.Type;

                if (p_enveloppe.Action == "POST")
                {
                    try
                    {
                        DSED_M06_ComptesBancaires_Entities.Compte compte = new DSED_M06_ComptesBancaires_Entities.Compte(compteDTO.Type);

                        this.m_manipulationComptes.CreerCompteBancaire(compte);
                    }
                    catch(ArgumentNullException exception)
                    {
                        p_enveloppeLettreMorte.Description = p_enveloppeLettreMorte.Description + exception.Message + "; ";
                    }
                    catch(InvalidDataException exception)
                    {
                        p_enveloppeLettreMorte.Description = p_enveloppeLettreMorte.Description + exception.Message + "; ";
                    }
                }
                else if (p_enveloppe.Action == "PUT")
                {
                    try
                    {
                        compteDTO.Numero = new Guid(messageCompte.Numero.ToString());
                        DSED_M06_ComptesBancaires_Entities.Compte compte = new DSED_M06_ComptesBancaires_Entities.Compte(compteDTO.Numero, compteDTO.Type);

                        this.m_manipulationComptes.ModifierCompteBancaire(compte);
                    }
                    catch (ArgumentNullException exception)
                    {
                        p_enveloppeLettreMorte.Description = p_enveloppeLettreMorte.Description + exception.Message + "; ";
                    }
                    catch (InvalidDataException exception)
                    {
                        p_enveloppeLettreMorte.Description = p_enveloppeLettreMorte.Description + exception.Message + "; ";
                    }
                }
                else
                {
                    p_enveloppeLettreMorte.Description = p_enveloppeLettreMorte.Description + "Action non valide; ";
                }
            }
            else if(p_enveloppe.MessageTransaction != null)
            {
                p_enveloppeLettreMorte.Description = p_enveloppeLettreMorte.Description + "TypeMessage non coherent; ";
            }
            else
            {
                p_enveloppeLettreMorte.Description = p_enveloppeLettreMorte.Description + "Message est null; ";
            }
        }
    }
}
