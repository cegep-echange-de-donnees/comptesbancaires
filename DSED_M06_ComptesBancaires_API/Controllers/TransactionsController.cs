﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using DSED_M06_ComptesBancaires_Buisness;
using DSED_M06_ComptesBancaires_FileMessages;
using DSED_M06_ComptesBancaires_Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DSED_M06_ComptesBancaires_API.Controllers
{
    [Route("api/transactions")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private ManipulationTransactions m_manipulationTransactions;
        public TransactionsController(ManipulationTransactions p_manipulationTransactions)
        {
            if(p_manipulationTransactions == null)
            {
                throw new ArgumentNullException(nameof(p_manipulationTransactions));
            }

            this.m_manipulationTransactions = p_manipulationTransactions;
        }
        // GET: api/transactions
        [HttpGet]
        [ProducesResponseType(200)]
        public ActionResult<IEnumerable<Models.Transaction>> Get()
        {
            List<Models.Transaction> listeTransactionsModel = this.m_manipulationTransactions.ObtenirListeTransactions()
                                                                                             .Select(transaction => new Models.Transaction(transaction))
                                                                                             .ToList();

            return Ok(listeTransactionsModel);
        }

        // GET api/transactions/{id}
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public ActionResult<Models.Transaction> Get(Guid id)
        {
            try
            {
                DSED_M06_ComptesBancaires_Entities.Transaction transaction = this.m_manipulationTransactions.RechercherTransaction(id);

                if (transaction == null)
                {
                    return NotFound();
                }

                Models.Transaction transactionModel = new Models.Transaction(transaction);

                return Ok(transactionModel);
            }
            catch(ArgumentNullException)
            {
                return BadRequest();
            }
        }

        // POST api/transactions
        [HttpPost]
        [ProducesResponseType(202)]
        [ProducesResponseType(400)]
        public ActionResult Post([FromBody] Models.Transaction transactionModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                DSED_M06_ComptesBancaires_Entities.Transaction transaction = transactionModel.VersEntite();
                this.m_manipulationTransactions.EnvoyerMessageTransaction(transaction, "POST");
            }
            catch(ArgumentNullException)
            {
                return BadRequest();
            }
            catch(InvalidDataException)
            {
                return BadRequest();
            }
            catch(ArgumentOutOfRangeException)
            {
                return BadRequest();
            }

            return Accepted();
        }

        // PUT api/transactions/{id}
        [HttpPut("{id}")]
        [ProducesResponseType(403)]
        public ActionResult Put(Guid id, [FromBody] Models.Transaction transactionModel)
        {
            return StatusCode(403);
        }

        // DELETE api/transactions/{id}
        [HttpDelete("{id}")]
        [ProducesResponseType(403)]
        public ActionResult Delete(Guid id)
        {
            return StatusCode(403);
        }
    }
}
