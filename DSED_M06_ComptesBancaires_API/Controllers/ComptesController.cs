﻿using Microsoft.AspNetCore.Mvc;
using System.Linq;
using DSED_M06_ComptesBancaires_Buisness;
using DSED_M06_ComptesBancaires_FileMessages;
using DSED_M06_ComptesBancaires_Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DSED_M06_ComptesBancaires_API.Controllers
{
    [Route("api/comptes")]
    [ApiController]
    public class ComptesController : ControllerBase
    {
        private ManipulationComptes m_manipulationComptes;
        public ComptesController(ManipulationComptes p_manipulationComptes)
        {
            if(p_manipulationComptes == null)
            {
                throw new ArgumentNullException(nameof(p_manipulationComptes));
            }

            this.m_manipulationComptes = p_manipulationComptes;
        }
        // GET: api/comptes
        [HttpGet]
        [ProducesResponseType(200)]
        public ActionResult<IEnumerable<Models.Compte>> Get()
        {
            List<Models.Compte> listeComptesBancaires = this.m_manipulationComptes.ObtenirListeComptesBancaires()
                                                                    .Select(compte => new Models.Compte(compte))
                                                                    .ToList();

            return Ok(listeComptesBancaires);
        }

        // GET api/comptes/{id}
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public ActionResult<Models.Compte> Get(Guid id)
        {
            try
            {
                DSED_M06_ComptesBancaires_Entities.Compte compteBancaire = this.m_manipulationComptes.RechercherCompteBancaire(id);

                if (compteBancaire == null)
                {
                    return NotFound();
                }

                Models.Compte compteBancaireModel = new Models.Compte(compteBancaire);

                return Ok(compteBancaireModel);
            }
            catch(ArgumentNullException)
            {
                return BadRequest();
            }
        }

        // POST api/comptes
        [HttpPost]
        [ProducesResponseType(202)]
        [ProducesResponseType(400)]
        public ActionResult Post([FromBody] Models.Compte compteBancaireModel)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                DSED_M06_ComptesBancaires_Entities.Compte compte = compteBancaireModel.VersEntite();
                this.m_manipulationComptes.EnvoyerMessageCompte(compte, "POST");
            }
            catch (ArgumentNullException)
            {
                return BadRequest();
            }
            catch(InvalidDataException)
            {
                return BadRequest();
            }
            catch(InvalidOperationException)
            {
                return BadRequest();
            }

            return Accepted();
        }

        // PUT api/comptes/{id}
        [HttpPut("{id}")]
        [ProducesResponseType(202)]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        public ActionResult Put(Guid id, [FromBody] Models.Compte compteBancaireModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            Guid compteBancaireModelId = new Guid(compteBancaireModel.Numero);

            if(id != compteBancaireModelId)
            {
                return BadRequest();
            }

            try
            {
                DSED_M06_ComptesBancaires_Entities.Compte compte = compteBancaireModel.VersEntite();
                this.m_manipulationComptes.EnvoyerMessageCompte(compte, "PUT");
            }
            catch(ArgumentNullException)
            {
                return BadRequest();
            }
            catch(InvalidDataException)
            {
                return BadRequest();
            }
            catch (InvalidOperationException)
            {
                return BadRequest();
            }

            return Accepted();
        }

        // DELETE api/comptes/{id}
        [HttpDelete("{id}")]
        [ProducesResponseType(403)]
        public ActionResult Delete(Guid id)
        {
            return StatusCode(403);
        }
    }
}
