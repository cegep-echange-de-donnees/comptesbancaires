﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DSED_M06_ComptesBancaires_Entities;

namespace DSED_M06_ComptesBancaires_API.Data
{
    [Table("Transaction")]
    public class Transaction
    {
        [Key]
        public Guid transactionId { get; set; }
        [Required]
        [Column]
        public Guid numeroCompte { get; set; }
        [Required]
        [Column]
        public string type { get; set; }
        [Required]
        [Column]
        public DateTime? date { get; set; }
        [Required]
        [Column]
        public decimal montant { get; set; }
        public Transaction()
        {
            ;
        }
        public Transaction(DSED_M06_ComptesBancaires_Entities.Transaction p_transaction)
        {
            if(p_transaction == null)
            {
                throw new ArgumentNullException(nameof(p_transaction));
            }
            this.transactionId = p_transaction.Id;
            this.numeroCompte = p_transaction.NumeroCompte;
            this.type = p_transaction.Type;
            this.date = p_transaction.Date;
            this.montant = p_transaction.Montant;
        }
        public DSED_M06_ComptesBancaires_Entities.Transaction VersEntite()
        {
            return new DSED_M06_ComptesBancaires_Entities.Transaction(this.transactionId, this.numeroCompte, this.type, this.date, this.montant);
        }
    }
}
