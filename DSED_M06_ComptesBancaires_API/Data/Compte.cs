﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DSED_M06_ComptesBancaires_Entities;

namespace DSED_M06_ComptesBancaires_API.Data
{
    [Table("Compte")]
    public class Compte
    {
        [Key]
        public Guid compteId { get; set; }
        [Required]
        [Column]
        public string type { get; set; }
        public Compte()
        {
            ;
        }
        public Compte(DSED_M06_ComptesBancaires_Entities.Compte p_compte)
        {
            if(p_compte == null)
            {
                throw new ArgumentNullException(nameof(p_compte));
            }
            this.compteId = p_compte.Numero;
            this.type = p_compte.Type;
        }
        public DSED_M06_ComptesBancaires_Entities.Compte VersEntite()
        {
            return new DSED_M06_ComptesBancaires_Entities.Compte(this.compteId, this.type);
        }
    }
}
