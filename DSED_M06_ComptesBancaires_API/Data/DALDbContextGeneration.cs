﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore.Internal;
using System.IO;

namespace DSED_M06_ComptesBancaires_API.Data
{
    public static class DALDbContextGeneration
    {
        private static DbContextOptions<ApplicationDbContext> _dbContextOptions;
        private static PooledDbContextFactory<ApplicationDbContext> _pooledDbContextFactory;

        static DALDbContextGeneration()
        {
            string? path = Directory.GetParent(Environment.CurrentDirectory)?.Parent?.Parent?.Parent?
                                          .GetDirectories("DSED_M06_ComptesBancaires_API")?
                                          .Select(directory => directory)?.SingleOrDefault()?.FullName;
            IConfigurationRoot configuration =
                new ConfigurationBuilder()
                    .SetBasePath(path)
                    .AddJsonFile("appsettings.json", false)
                    .Build();

            _dbContextOptions = new DbContextOptionsBuilder<ApplicationDbContext>()
                    .UseSqlServer(configuration.GetConnectionString("DefaultConnection"))
                    .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking)
#if DEBUG
                    .LogTo(message => Debug.WriteLine(message), LogLevel.Information)
                    .EnableSensitiveDataLogging()
#endif
                    .Options;

            _pooledDbContextFactory = new PooledDbContextFactory<ApplicationDbContext>(_dbContextOptions);
        }
        public static ApplicationDbContext ObtenirApplicationDBContext()
        {
            return _pooledDbContextFactory.CreateDbContext();
        }
    }
}
