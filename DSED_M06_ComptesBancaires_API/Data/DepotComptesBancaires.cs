﻿using DSED_M06_ComptesBancaires_Entities;

namespace DSED_M06_ComptesBancaires_API.Data
{
    public class DepotComptesBancaires : IDepotComptesBancaires
    {
        private ApplicationDbContext m_context;
        public DepotComptesBancaires(ApplicationDbContext p_applicationDbContext)
        {
            if(p_applicationDbContext == null)
            {
                throw new ArgumentNullException(nameof(p_applicationDbContext));
            }

            this.m_context = p_applicationDbContext;
        }
        public void CreerCompteBancaire(DSED_M06_ComptesBancaires_Entities.Compte p_compte)
        {
            if(p_compte == null)
            {
                throw new ArgumentNullException(nameof(p_compte));
            }

            Compte compte = new Compte(p_compte);

            this.m_context.Add(compte);
            this.m_context.SaveChanges();
            this.m_context.ChangeTracker.Clear();
        }

        public void CreerTransaction(DSED_M06_ComptesBancaires_Entities.Transaction p_transaction)
        {
            if (p_transaction == null)
            {
                throw new ArgumentNullException(nameof(p_transaction));
            }

            Transaction transaction = new Transaction(p_transaction);

            this.m_context.Add(transaction);
            this.m_context.SaveChanges();
            this.m_context.ChangeTracker.Clear();
        }

        public void ModifierCompteBancaire(DSED_M06_ComptesBancaires_Entities.Compte p_compte)
        {
            if(p_compte == null)
            {
                throw new ArgumentNullException(nameof(p_compte));
            }

            DSED_M06_ComptesBancaires_Entities.Compte compteEntite = this.RechercherCompteBancaire(p_compte.Numero);

            if(compteEntite == null)
            {
                throw new InvalidOperationException(nameof(compteEntite));
            }

            Compte compte = new Compte(compteEntite);
            
            this.m_context.Update(compte);
            this.m_context.SaveChanges();
            this.m_context.ChangeTracker.Clear();
        }

        public IEnumerable<DSED_M06_ComptesBancaires_Entities.Compte> ObtenirListeComptesBancaires()
        {
            IQueryable<Compte> requete = this.m_context.Comptes;
            return requete.Select(compte => compte.VersEntite()).ToList();
        }

        public IEnumerable<DSED_M06_ComptesBancaires_Entities.Transaction> ObtenirListeTransactions()
        {
            IQueryable<Transaction> requete = this.m_context.Transactions;
            return requete.Select(transaction => transaction.VersEntite()).ToList();
        }

        public DSED_M06_ComptesBancaires_Entities.Compte RechercherCompteBancaire(Guid p_numeroCompte)
        {
            if (p_numeroCompte == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(p_numeroCompte));
            }

            return this.m_context.Comptes.Where(compte => compte.compteId == p_numeroCompte).Select(compte => compte.VersEntite()).SingleOrDefault();
        }

        public DSED_M06_ComptesBancaires_Entities.Transaction RechercherTransaction(Guid p_id)
        {
            if (p_id == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(p_id));
            }

            return this.m_context.Transactions.Where(transaction => transaction.transactionId == p_id).Select(transaction => transaction.VersEntite()).SingleOrDefault();
        }
    }
}
