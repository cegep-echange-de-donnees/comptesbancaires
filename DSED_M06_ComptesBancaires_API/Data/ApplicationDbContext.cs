﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DSED_M06_ComptesBancaires_API.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options): base(options)
        {
            ;
        }

        public DbSet<Compte> Comptes { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public List<Transaction> ObtenirTransactionDuCompte(Guid p_numeroCompte)
        {
            if(p_numeroCompte == Guid.Empty)
            {
                throw new ArgumentNullException(nameof(p_numeroCompte));
            }

            List<Transaction> transactions = new List<Transaction>();
            transactions = this.Transactions.FromSqlRaw("SELECT * FROM [Transaction] WHERE numeroCompte = {0}", p_numeroCompte.ToString()).ToList();

            return transactions;
        }
    }
}