﻿using DSED_M06_ComptesBancaires_Entities;

namespace DSED_M06_ComptesBancaires_API.Models
{
    public class Transaction
    {
        public string Id { get; set; }
        public Guid NumeroCompte { get; set; }
        public string Type { get; set; }
        public DateTime? Date { get; set; }
        public decimal Montant { get; set; }
        public Transaction()
        {
            ;
        }
        public Transaction(DSED_M06_ComptesBancaires_Entities.Transaction p_transaction)
        {
            if (p_transaction == null)
            {
                throw new ArgumentNullException(nameof(p_transaction));
            }
            this.Id = p_transaction.Id.ToString();
            this.NumeroCompte = new Guid(p_transaction.NumeroCompte.ToString());
            this.Type = p_transaction.Type;
            this.Date = p_transaction.Date;
            this.Montant = p_transaction.Montant;
        }
        public DSED_M06_ComptesBancaires_Entities.Transaction VersEntite()
        {
            return new DSED_M06_ComptesBancaires_Entities.Transaction(new Guid(this.NumeroCompte.ToString()), this.Type, this.Date, this.Montant);
        }
    }
}
