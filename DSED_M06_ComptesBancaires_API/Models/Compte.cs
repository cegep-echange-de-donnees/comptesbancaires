﻿using DSED_M06_ComptesBancaires_Entities;

namespace DSED_M06_ComptesBancaires_API.Models
{
    public class Compte
    {
        public string Numero { get; set; }
        public string Type { get; set; }
        public Compte()
        {
            ;
        }
        public Compte(DSED_M06_ComptesBancaires_Entities.Compte p_compte)
        {
            if (p_compte == null)
            {
                throw new ArgumentNullException(nameof(p_compte));
            }
            this.Numero = p_compte.Numero.ToString();
            this.Type = p_compte.Type;
        }
        public DSED_M06_ComptesBancaires_Entities.Compte VersEntite()
        {
            return new DSED_M06_ComptesBancaires_Entities.Compte(this.Type);
        }
    }
}
